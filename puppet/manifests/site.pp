## puppetlite.pp ##

# This file (puppetlite/puppet/manifests/puppetlite.pp) is the main entry point
# used to prepare a node for running puppet.

## Active Configurations ##

node default {
  # For puppetlite, we can not be confident in the ability to change the hostname (eg, docker). And anyhow,
  # changing the hostname then requires a second run for full configuration.
  # Here, we ignore all hostname data, and assign role based on Hiera lookup (Probably mac address)
  info("initial pre-run to configure puppetlite")
  include puppetlite
}
