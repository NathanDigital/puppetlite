# == Class: puppetlite::target_external_facts
class puppetlite::target_external_facts inherits puppetlite {
  # Place a set of external facts in the external fact dir acording to the target hiera.

  $home = $facts['target_home']

  file{['/etc/facter', '/etc/facter/facts.d']:
    ensure => 'directory',
  }

  exec{ 'target_external_facts':
    command => "/bin/cp -t /etc/facter/facts.d/ `/usr/bin/puppet lookup puppetlite::external_facts --render-as s`",
    require => File['/etc/facter/facts.d'],
    cwd => "${home}/puppet/lib/external_facts"
  }
}


