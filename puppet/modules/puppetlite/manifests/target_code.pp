# == Class: puppetlite::target_code
class puppetlite::target_code inherits puppetlite {
  # this class fetched our target code repo. Unfortunatly this early we can't rely on puppet modules, so it nust be done by hand

  $source = $facts['target_code_source']
  $home = $facts['target_home']

  # fetch the branch required
  if $facts['target_branch'] {
    $branch = $facts['target_branch']
  }
  else {
    $branch = lookup({  'name'  => 'target_branch',
                        'default_value' => 'master',
    })
  }
  case $facts['os']['family'] {
    "Debian": {
      exec { 'target_clone':
        command => "/usr/bin/git clone ${source} ${home}",
        creates => "${home}/README.md"
      }

      exec { 'target_branch':
        command => "/usr/bin/git checkout ${branch}",
        unless => "/usr/bin/git status |grep 'On branch' | grep ${branch}",
        cwd => $home,
        require => Exec['target_clone'],
      }

      exec { 'target_pull':
        command => '/usr/bin/git pull --ff-only --quiet',
        # todo, puppet resource should only report change if new data is pulled
        # unless => '/usr/bin/git pull --ff-only --dry-run| grep Already up to date '
        cwd => $home,
        require => Exec['target_branch']
      }
    }
    "windows": {
      exec { 'target_clone':
        command => "C:/archives/portablegit/bin/git clone ${source} ${home}",
        creates => "C:${home}/README.md",
        provider => powershell,
      }

      exec { 'target_branch':
        command => "C:/archives/portablegit/bin/git checkout ${branch}",
        unless => "C:/archives/portablegit/bin/git status | Select-String -Pattern 'On branch' | Select-String -Pattern  ${branch}",
        cwd => "C:${home}",
        require => Exec['target_clone'],
        provider => powershell,
      }

      exec { 'target_pull':
        command => 'C:/archives/portablegit/bin/git pull --ff-only --quiet',
        # todo, puppet resource should only report change if new data is pulled
        # unless => '/usr/bin/git pull --ff-only --dry-run| grep Already up to date '
        cwd => "C:${home}",
        require => Exec['target_branch'],
        provider => powershell,
      }
    }
  }
}
