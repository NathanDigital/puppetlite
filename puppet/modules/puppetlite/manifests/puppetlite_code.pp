# == Class: puppetlite::puppetlite_code
class puppetlite::puppetlite_code inherits puppetlite {
  # this class fetched our target code repo. Unfortunatly this early we can't rely on puppet modules, so it nust be done by hand

  $source = $facts['puppetlite_code_source']
  $home = $facts['puppetlite_home']

  # fetch the branch required
  if $facts['puppetlite_branch'] {
    $branch = $facts['puppetlite_branch']
  }
  else {
    $branch = lookup({  'name'  => 'puppetlite::branch',
                        'default_value' => 'master',
    })
  }

  exec { 'puppetlite_clone':
    command => "/usr/bin/git clone ${source} ${home}",
    creates => "${home}/README.md"
  }

  exec { 'puppetlite_branch':
    command => "/usr/bin/git checkout ${branch}",
    unless => "/usr/bin/git status |grep 'On branch' | grep ${branch}",
    cwd => $home,
    require => Exec['puppetlite_clone'],
  }

  exec { 'puppetlite_pull':
    command => '/usr/bin/git pull --ff-only --quiet',
    # todo, puppet resource should only report change if new data is pulled
    # unless => '/usr/bin/git pull --ff-only --dry-run| grep Already up to date '
    cwd => $home,
    require => Exec['puppetlite_branch']
  }
}
