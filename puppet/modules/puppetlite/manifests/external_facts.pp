# == Class: puppetlite::external_facts
class puppetlite::external_facts inherits puppetlite {
  # Place a set of external facts in the external fact dir acording to the target hiera.
  $target_home = lookup('target_home')
  $fact_files = lookup('target_external_facts', {merge => 'unique'})
  $fact_files.each |String $fact_file| {
    file {"/etc/facter/facts.d/${fact_file}" :
      ensure => file,
      source => "${target_home}/puppet/lib/external_facts/${fact_file}",
      mode => '755',
      require => Exec['sneaky_replace_external_facts'],
    }
  }
}


# Next up, populate hiera with fact file names
