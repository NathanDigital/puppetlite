# == Class: puppetlite::target_hiera
class puppetlite::target_hiera inherits puppetlite {
  # configure the default locations with simlinks to target hiera so we can use pupper lookup to query target data

  $home = $facts['target_home']

  # Lets use the target's lite version of hiera, with no custom facts
  case $facts['os']['family'] {
    "windows": {
      file { 'C:/ProgramData/PuppetLabs/puppet/etc/hiera.yaml':
        ensure => 'link',
        target => "${home}/puppet/hiera_puppetlite.yaml",
      }

      file { 'C:/ProgramData/PuppetLabs/puppet/etc/data':
        ensure => 'link',
        target => "${home}/puppet/data",
      }
    }
    "Debian": {
      file { '/etc/puppet/hiera.yaml':
        ensure => 'link',
        target => "${home}/puppet/hiera_puppetlite.yaml",
      }

      file { '/etc/puppet/data':
        ensure => 'link',
        target => "${home}/puppet/data",
      }
    }
  }
}


