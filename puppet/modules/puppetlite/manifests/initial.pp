# == Class: puppetlite::initial
class puppetlite::initial inherits puppetlite {

  file{'/initial':
    ensure =>  'directory',
    owner   => 'root',
    recurse => 'true',
    source => "file:///opt/puppetlite/initial",
    purge => 'false',
  }

  exec{'set_key_permissions':
    command => "/bin/chmod 600 /initial/ssh/*",
    require => File['/initial'],
    unless => "/usr/bin/stat -c '%a' /initial/ssh/puppetlite |/bin/grep -q '600'",
  }
}