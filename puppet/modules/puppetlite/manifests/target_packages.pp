# == Class: puppetlite::target_packages
class puppetlite::target_packages inherits puppetlite {
  # install a set of packages acording to the target hiera.
  exec{ target_packages:
    command => "/usr/bin/apt-get install `puppet lookup puppetlite::packages --render-as s` -y"
  }
}


