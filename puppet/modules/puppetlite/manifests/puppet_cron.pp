# == Class: puppetlite::puppet_cron
class puppetlite::puppet_cron inherits puppetlite {
  # Generate a random number so that evert box dosn't run puppet at the same time
  $num = fqdn_rand(29, $facts['macaddress'])

  $home = $facts['puppetlite_home']

  case $facts['os']['family'] {
    "Debian": {
      package { 'cron':
        ensure => 'installed',
      }

      $num2 = $num + 30


      cron { 'puppet_run':
        command => "${home}/initial/run_puppetlite.sh; ${home}/initial/run_puppet.sh",
        user    => 'root',
        minute  => [$num, $num2],
        require => Package['cron'],
      }
    }
    "windows": {
      # create scheduled task for puppet cron

      $job_cmd =  @("EOT"/$)
        # Create user
        \$PlainPassword = -join ((65..90) + (97..122) | Get-Random -Count 12 | % {[char]\$_})
        \$SecurePassword = \$PlainPassword | ConvertTo-SecureString -AsPlainText -Force
        New-LocalUser "puppetlite_cron" -Password \$SecurePassword -FullName "Puppetlite Automation cron" -Description "Automation user for puppetlite" -AccountNeverExpires -PasswordNeverExpires -UserMayNotChangePassword
        Add-LocalGroupMember -Group "Administrators" -Member "puppetlite_cron" 
        # Create task
        \$repeat = (New-TimeSpan -Minutes 30);
        \$time = New-ScheduledTaskTrigger -Once -At (Get-Date).Date  -RepetitionInterval \$repeat;
        #\$principle = New-ScheduledTaskPrincipal -UserId "puppetlite_cron" -LogonType password -RunLevel Highest;
        \$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument "${home}/initial/windows10.ps1";
        Register-ScheduledTask -TaskName "puppetlite_cron" -Trigger \$time -User "puppetlite_cron" -Password \$PlainPassword -Action \$action;
        if (-not \$(start-scheduledtask -TaskName "puppetlite_cron")) { exit 1 };
        | EOT

      exec { 'puppetlite_scheduled_job':
        command => $job_cmd,
        unless => 'if (-not $(start-scheduledtask -TaskName "puppetlite_cron")) { exit 1 }',
        provider => powershell
      }

    }
  }
}
