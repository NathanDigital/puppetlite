# == Class: puppetlite::watchdog
class puppetlite::watchdog inherits puppetlite {
  package {'watchdog':
    ensure => 'installed'
  }
  file{'/etc/watchdog.conf':
    path    => '/etc/watchdog.conf',
    ensure =>  'file',
    owner   => 'root',
    source => "puppet:///modules/puppetlite/watchdog.conf",
    require => Package['watchdog'],
  }
  service{'watchdog':
    ensure => 'running',
    subscribe => File['/etc/watchdog.conf'],
    require => Package['watchdog'],
  }
}