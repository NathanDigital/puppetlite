# == Class: puppetlite::deploy_key
class puppetlite::deploy_key inherits puppetlite {

  $source = $facts['puppetlite_code_source']
  $home = $facts['puppetlite_home']

  case $facts['os']['family'] {
    "Debian": {

      file { 'root_ssh_dir':
        ensure  => 'directory',
        path    => "/root/.ssh",
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
      }

      file { 'puppetlite_deploy_private_key_root':
        ensure  => 'file',
        path    => '/root/.ssh/id_rsa',
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
        content  => $facts['target_code_ssh_key'],
        require => File['root_ssh_dir'],
      }

      # Lets avoid the question of accepting gitlab's ssh host key 
      file { 'ssh_config':
        ensure => 'file',
        path => '/root/.ssh/config',
        content => 'Host gitlab.com
        StrictHostKeyChecking no',
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
        require => File['root_ssh_dir'],
      }
    }
    'windows': {
      $homepath = $facts['homepath']

      file { 'ssh_dir':
        path => "c:${homepath}/.ssh",
        ensure => directory,
      }

      file { 'target_deploy_key':
        content => $facts['target_code_ssh_key'],
        path => "c:${homepath}/.ssh/id_rsa",
        ensure => file,
        require => File['ssh_dir'],
      }

      file { 'target_ssh_config':
        ensure => 'file',
        path => "c:${homepath}/.ssh/config",
        content => 'Host gitlab.com
        StrictHostKeyChecking no',
        require => File['ssh_dir'],
      }

    }
  }
}
