#!/usr/bin/env bash
#fetch our puppet code
eval $(ssh-agent)
mkdir ~/.ssh
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
ssh-keyscan github.com >> ~/.ssh/known_hosts
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
ssh-add /initial/ssh/puppetlite

function fallback {
    timeout 60 wget -O /opt/puppetlite.tar.bz http://10.0.0.9/puppetlite.tar.bz
    cd /opt
    tar -xjf /opt/puppetlite.tar.bz
}

timeout 60 git clone git@gitlab.com:Rockan/puppetlite.git /opt/puppetlite --depth=1 --no-single-branch || fallback

#clean up ssh-agent process
pkill ssh-agent

#fetch puppet modules
# commenting for now as librian-puppet is broken
# cd /opt/puppetlite/puppet/contrib/
# librarian-puppet install --clean
