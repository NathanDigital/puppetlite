#!/usr/bin/env bash
# Master file to install and start puppetlite (currently assumes ubuntu)
./ubuntu.sh
./fetch_puppet.sh
./update_puppet.sh
./run_puppet.sh
