#!/usr/bin/env bash
#update our puppet code
eval $(ssh-agent)
mkdir ~/.ssh
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
ssh-add /initial/ssh/puppetlite
cd /opt/puppetlite
timeout 30 git fetch origin
git checkout master
git rebase

# Now we switch to the branch indicated by the fact "branch"

#note this is a bodge, because ubuntu's facter version is old and can't support setting your own external facts dir
mkdir -p /etc/facter/facts.d/
cp /opt/puppetlite/puppet/lib/external_facts/*  /etc/facter/facts.d/
BRANCH=$(facter branch)
git checkout $BRANCH
git rebase origin/$BRANCH

#clean up ssh-agent process
pkill ssh-agent
#fetch puppet modules
# commenting for now as librian-puppet is broken
# cd /opt/puppetlite/puppet/contrib/
# librarian-puppet install --clean
