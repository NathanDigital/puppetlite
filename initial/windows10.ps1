
# prepare for first run
# This script must be run as administrator or higher
# don't forget to run "Set-ExecutionPolicy -ExecutionPolicy Unrestricted" to enable scripts
# Don't forget to put facts into c:/archives/facts.d/* that contain puppetlite_home, puppetlite_code_source, puppetlite_branch and
# target_home, target_code_source, target_branch, and target_code_ssh_key.

# this couter increments when we do anything requiring a reboot

# Start a new transcript
$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Continue"
Start-Transcript -IncludeInvocationHeader -OutputDirectory "C:\archives"

$reboot_counter = 0
if (-not $(Test-Path c:\archives) ) { Write-Host 'Making archives directory...'; mkdir c:\archives}
if (-not $(Test-Path  "c:\Program Files\Puppet Labs\Puppet\bin\facter.bat") ) { 
    Write-Host 'Fetching puppet...'; 
    wget http://rockan.nathan.digital.s3-website.eu-central-1.amazonaws.com/puppet-agent-5.5.1-x64.msi -OutFile c:\archives\puppet-agent-5.5.1-x64.msi;
    Write-Host 'Installing puppet...';
    msiexec /qn /norestart /i c:\archives\puppet-agent-5.5.1-x64.msi PUPPET_MASTER_SERVER=puppet PUPPET_AGENT_STARTUP_MODE=Disabled;
    $reboot_counter++
    Write-Host 'sleeping 15s...'
    Start-Sleep -s 15
}

if (-not $(Test-Path "C:\archives\portablegit\bin\git.exe")) {
    Write-Host 'fetching git...';
    wget http://rockan.nathan.digital.s3-website.eu-central-1.amazonaws.com/portablegit_rockan.zip -OutFile c:\archives\portablegit_rockan.zip
    Write-Host 'unpacking git...';
    Expand-Archive -LiteralPath c:\archives\portablegit_rockan.zip -DestinationPath C:\archives
    Write-Host 'copying ssh keys for git...';
    mkdir  $env:HOMEPATH/.ssh
    cp C:\archives\portablegit\.ssh\* $env:HOMEPATH\.ssh\ 
    $reboot_counter++
    Write-Host 'sleeping 15s...'
    Start-Sleep -s 15
}


if (-not $(Get-ScheduledTaskInfo -erroraction 'silentlycontinue' puppetlite_boot)) { 
    
    Write-Host 'create puppetlite automation user...';
    $PlainPassword = -join ((65..90) + (97..122) | Get-Random -Count 12 | % {[char]$_})
    Write-Host "Password generated is $PlainPassword"
    $SecurePassword = $PlainPassword | ConvertTo-SecureString -AsPlainText -Force
    New-LocalUser "puppetlite_boot" -Password $SecurePassword -FullName "Puppetlite Automation first boot" -Description "Automation user for puppetlite_boot" -AccountNeverExpires -PasswordNeverExpires -UserMayNotChangePassword
    Add-LocalGroupMember -Group "Administrators" -Member "puppetlite_boot" 
    
    Write-Host 'Setting boot scheduled task...';
    $time = New-ScheduledTaskTrigger -AtStartup;
    $principle = New-ScheduledTaskPrincipal -UserId "puppetlite_boot" -LogonType password -RunLevel Highest;
    $action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument 'C:\archives\windows10.ps1';
    Register-ScheduledTask -TaskName "puppetlite_boot" -Trigger $time -User "puppetlite_boot" -Action $action -Password $PlainPassword;

    $reboot_counter++
    Write-Host 'sleeping 15s...'
    Start-Sleep -s 15
}

if (-not $(Test-Path "C:\ProgramData\PuppetLabs\facter\facts.d\puppetlite.yaml")) {
    Write-Host 'copy user facts to facts.d...'
    # need to remove bodgy hardcoded source
    cp  c:/archives/facts.d/* C:\ProgramData\PuppetLabs\facter\facts.d\
    $reboot_counter++
}

if ($reboot_counter -ne 0 ) {
    Write-Host 'restarting computer...';
    Restart-Computer
}


# If we get here, then all the previous steps are done and we can continue


Write-Host 'making opt dir...'
if (-not $(Test-Path "C:\opt")) {mkdir /opt}

Write-Host 'sleeping 15s...'
Start-Sleep -s 15

Write-Host 'clone puppetlite repo'
C:\archives\portablegit\bin\git clone -b $(facter puppetlite_branch) $(facter puppetlite_code_source) $(facter puppetlite_home)


Write-Host 'run puppetlite to prepare environment...'
$PUPPETLITE_HOME = facter puppetlite_home
$os_family = facter os.family
# copy facts that come with puppetlite for this os.family
cp c:${PUPPETLITE_HOME}/puppet/files/fact_family/${os_family}/* C:/ProgramData/PuppetLabs/facter/facts.d/
# Install the powershell module 
puppet module install puppetlabs-powershell --version 2.1.5
# run puppet
puppet apply -v --hiera_config="c:${PUPPETLITE_HOME}/puppet/hiera.yaml" --modulepath="c:${PUPPETLITE_HOME}/puppet/modules;c:${PUPPETLITE_HOME}/puppet/site;C:/ProgramData/PuppetLabs/code/environments/production/modules" --detailed-exitcodes ${PUPPETLITE_HOME}/puppet/manifests/site.pp


# run main puppet run
# $TARGET_HOME = facter target_home
# $Env:FACTERLIB=${TARGET_HOME}/puppet/lib/custom_facts 
#puppet apply -v --hiera_config="${TARGET_HOME}/puppet/hiera.yaml" --modulepath="${TARGET_HOME}/puppet/modules:${TARGET_HOME}/puppet/contrib/modules:${TARGET_HOME}/puppet/site" ${TARGET_HOME}/puppet/manifests/site.pp


#puppet apply -v --hiera_config="C:\rockan\puppetlite\puppet\hiera.yaml" --modulepath="C:\rockan\puppetlite\puppet\modules;C:\rockan\puppetlite\puppet\contrib\modules;C:\rockan\puppetlite\puppet\site" C:\rockan\puppetlite\puppet\manifests\site.pp

