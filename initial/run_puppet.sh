#!/usr/bin/env bash
#set the path for custom facts
export FACTERLIB=/opt/puppetlite/puppet/lib/custom_facts
#drop external facts in the default directory
#note this is a bodge, because ubuntu's facter version is old and can't support setting your own external facts dir
mkdir -p /etc/facter/facts.d/
cp /opt/puppetlite/puppet/lib/external_facts/*  /etc/facter/facts.d/
#run puppet and set hiera and modules dirs
puppet apply -v \
--hiera_config="/opt/puppetlite/puppet/hiera.yaml" \
--modulepath="/opt/puppetlite/puppet/modules:/opt/puppetlite/puppet/contrib/modules:/opt/puppetlite/puppet/site" \
/opt/puppetlite/puppet/manifests/site.pp