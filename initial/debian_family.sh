#!/usr/bin/env bash
# Start puppetlite on Debian family
# Tested on ubuntu18.04, raspbian_streach 

#update the repos
apt-get update 

#install the stuff we need
apt-get install puppet git  -y

# copy puppetlite facts to facts.d and get facts
mkdir /etc/facter
cp -r /initial/facts.d /etc/facter/


PUPPETLITE_HOME=`facter puppetlite_home`
PUPPETLITE_BRANCH=`facter puppetlite_branch`
PUPPETLITE_CODE_SOURCE=`facter puppetlite_code_source`


# fetch puppetlite
git clone -b `facter puppetlite_branch` `facter puppetlite_code_source` ${PUPPETLITE_HOME}

#run puppetlite to prepare environment
puppet apply -v \
--hiera_config="${PUPPETLITE_HOME}/puppet/hiera.yaml" \
--modulepath="${PUPPETLITE_HOME}/puppet/modules" \
${PUPPETLITE_HOME}/puppet/manifests/site.pp

# run main puppet run
# TARGET_HOME=`facter target_home`
# export FACTERLIB=${TARGET_HOME}/puppet/lib/custom_facts
# puppet apply -v \
# --hiera_config="${TARGET_HOME}/puppet/hiera.yaml" \
# --modulepath="${TARGET_HOME}/puppet/modules:${TARGET_HOME}/puppet/contrib/modules:${TARGET_HOME}/puppet/site" \
# ${TARGET_HOME}/puppet/manifests/site.pp