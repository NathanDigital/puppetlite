#!/usr/bin/env bash
#this is the initial starter for puppetlite on ubuntu artful

#Get tools for updating our repos
cp /initial/sources.list /etc/apt/sources.list
apt-get update
apt-get install software-properties-common -y
#Add custom repo to get git, as the ubuntu artful version is broken
add-apt-repository ppa:git-core/ppa -y
apt-get update 


#install the stuff we actualy want
# don't need librarian-puppet for now
#apt-get install puppet git openssh-client librarian-puppet lshw  -y
apt-get install puppet git openssh-client systemd lshw  -y

