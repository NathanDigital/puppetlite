#!/usr/bin/env python3
import argparse
import subprocess

parser = argparse.ArgumentParser(
    description='This script starts puppetlite test instances with the various roles',
)

parser.add_argument('-i', action="store_true", default=False)
parser.add_argument('-r', action="store", type=str, default='blank', choices=['blank', 'miner', 'boot', 'splunk', 'mineproxy'])
parser.add_argument('-l', action="store_true", default=False)

opts = parser.parse_args()

# Lets construct our arguments
args = []
args.extend(['docker', 'run'])
if opts.i :
    args.append('-itP')
if opts.l:
    args.append('-v ~/git/puppetlite:/opt/puppetlite')
if opts.r == 'splunk':
    args.append('-p 8000:8000')
args.append("-h {0}.rockan.local".format(opts.r))
args.append('test')
if opts.i:
    args.append('bash')
elif opts.l:
    args.append('./run_puppet.sh')

print(' '.join(args))
subprocess.run(' '.join(args), shell=True, check=True)
