#!/usr/bin/env bash

for enc in `seq 1 80`;
  do
    for bay in `seq 1 16`;
      do
	printf bladee"$enc"b"$bay".rockan.local
        timeout 10 ssh -o StrictHostKeyChecking=no bladee"$enc"b"$bay".rockan.local  systemd-resolve --status  >/dev/null 2>&1
	exit_status=$?
	if [ $exit_status -eq 0 ]; then
	  printf " success\n"
	else
	  printf " failed\n"
	fi
        #sleep 1
      done
  done