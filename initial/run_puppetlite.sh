#!/usr/bin/env bash
# This run prepeares a machine for running puppet proper

# Install puppet
apt-get update
apt-get -y install puppet git


# Move the external facts aside, This puppet run will put them back
mv /etc/facter/facts.d /etc/facter/facts.d.donotrun 
# Run puppet and set hiera and modules dirs
puppet apply -v \
--hiera_config="/opt/puppetlite/puppet/puppetlite_hiera.yaml" \
--modulepath="/opt/puppetlite/puppet/modules:/opt/puppetlite/puppet/contrib/modules:/opt/puppetlite/puppet/site" \
/opt/puppetlite/puppet/manifests/puppetlite.pp

